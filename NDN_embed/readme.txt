=== Plugin Name ===
Contributors: Butler Raines, David Wallace
Donate link: http://newsinc.com/
Tags: video, news, ndn, newsinc, news distribution network
Requires at least: 1.0.0
Tested up to: 1.0.0
Stable tag: "trunk"
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin enables shortcode for embedding NDN video and a conversion tool to generate the shortcode from the traditional embed code.

== Description ==

Using the [NDN] short code will allow users to easily add video to their WordPress site from the industry leading News Distribution Network video library. 

From your account in the NDN control room at http://control.newsinc.com select the embed code for the product you want to put on your page. In your WordPress admin menu select the "NDN Video" tab.  Then select the "Conversion Tool" tab. Paste the embed code into the text box and click the "Convert" button. Copy the generated shortcode and paste it into your page or post.

The minimum required parameters in the shortcode are "type=", "widgetid=" and "trackinggroup=". 


== Installation ==

1. With your FTP program, upload the Plugin folder to the wp-content/plugins folder in your WordPress directory online. You can also search for and upload plugins from within your WP admin � click "Add New" from the Plug-ins submenu. 
2. Go to Plugins screen and find the newly uploaded Plugin in the list.
3. Click Activate Plugin to activate it.

== Frequently Asked Questions ==

= Do I have to have an NDN account to use the NDN plugin? =

Yes, you need to have an account at http://control.newsinc.com. 

= How do I become an NDN partner and get an account at http://control.newsinc.com? =

To become a NDN partner for video distribution please contact us at 404.962.7400.

== Screenshots ==

1. This is the page for the embed code conversion.
2. Conversion page with sample embed code pasted into text area.
3. Results of converting sample embed code.

== Changelog ==

= 1.0.0 =
* Original version.
