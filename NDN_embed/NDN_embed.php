<?php
/*
Plugin Name: Allow NDN javascript embeds.
version: 1.0.0
Description: Allows embedding of NDN javascript
Author: NDN
Author URI: http://www.newsinc.com/
*/

add_action( 'admin_menu', 'register_my_custom_menu_page' );

function register_my_custom_menu_page(){
	add_menu_page( 'NDN Shortcode Generator', 'NDN Video', 'edit_posts', 'NDN_embed/description.php', '', plugins_url( 'NDN_embed/images/icon.jpg' ), 77.123 );
	add_submenu_page( 'NDN_embed/description.php', 'NDN Embed Code Defaults', 'Default Settings', 'edit_posts', __FILE__, 'ndn_defaults_page' );
	add_submenu_page( 'NDN_embed/description.php', 'NDN Embed Code Conversion Tool', 'Conversion Tool', 'edit_posts', 'NDN_embed/conversion.php', '' );
}

function ndnv($atts) 
{
extract( shortcode_atts( array( 
	'id'			=> get_option('ndn_default_id'),
    'class'			=> get_option('ndn_default_class'),
    'style'         => '',
	'widgetid'      => get_option('ndn_default_widgetid'),
	'type'          => get_option('ndn_default_type'),
	'videoid'       => '', 
	'playlistid'    => '',
	'sitesection'   => get_option('ndn_default_sitesection'),
	'trackinggroup' => get_option('ndn_default_trackinggroup'),
    'rmmsoundon'    => '',
    'autoplay'      => '',
    'continuousplay'=> '',
), $atts ) );
$class = preg_replace('/[,]/',' ',$class);
if ($id == '')$id = 'ndn_player_'.mt_rand();
$ndn = '<div class="ndn_embed';
if ($class != '')$ndn = $ndn.' '.$class;
$ndn = $ndn.'" id="'.$id.'" ';
if ($type == "VideoPlayer/Single" && $style == '' && get_option('ndn_default_style') != ''){
	$style = get_option('ndn_default_style');
}
if ($style != ''){$ndn = $ndn.'style="'.$style.'" ';}
if ($widgetid != ''){$ndn = $ndn.'data-config-widget-id="'.$widgetid.'" ';}
if ($type != ''){$ndn = $ndn.'data-config-type="'.$type.'" ';}
if ($videoid != ''){$ndn = $ndn.'data-config-video-id="'.$videoid.'" ';}
if ($playlistid != ''){$ndn = $ndn.'data-config-playlist-id="'.$playlistid.'" ';}
if ($sitesection != ''){$ndn = $ndn.'data-config-site-section="'.$sitesection.'" ';}
if ($trackinggroup != ''){$ndn = $ndn.'data-config-tracking-group="'.$trackinggroup.'" ';}
if ($rmmsoundon != ''){$ndn = $ndn.'data-config-rmm-sound-on="'.$rmmsoundon.'" ';}
if ($autoplay != ''){$ndn = $ndn.'data-config-auto-play="'.$autoplay.'" ';}
if ($continuousplay != ''){$ndn = $ndn.'data-config-continuous-play="'.$continuousplay.'" ';}
$ndn = $ndn.'></div><script type="text/javascript" async src="//launch.newsinc.com/js/embed.js" id="_nw2e-js"></script>';

return $ndn;
}

add_shortcode('NDN', 'ndnv');

function ndn_setOptions(){
	update_option('ndn_default_type',$_POST['default_type']);
	update_option('ndn_default_trackinggroup',$_POST['default_trackinggroup']);
	update_option('ndn_default_widgetid',$_POST['default_widgetid']);
	update_option('ndn_default_id',$_POST['default_id']);
	update_option('ndn_default_class',$_POST['default_class']);
	update_option('ndn_default_width',$_POST['default_width']);
	update_option('ndn_default_height',$_POST['default_height']);
	$ndn_style = 'width:'.$_POST['default_width'].'px;height:'.$_POST['default_height'].'px;';
	update_option('ndn_default_style',$ndn_style);
	update_option('ndn_default_sitesection',$_POST['default_sitesection']);
}
if (isset($_POST['submit'])){
	ndn_setOptions();
}

function ndn_defaults_page(){
?>
<div class="wrap">
<h2>How it works</h2>

<p>The NDN shortcode has been developed to easily embed the NDN products into a WordPress page. We have made default settings available below to simplify the process.</p>

<p>There are a minimum of three attributes required for the NDN shortcode to work:</p>

<blockquote><strong>
<p>type</p>
<p>trackinggroup</p>
<p>widgetid</p>
</strong></blockquote>

<h2>Type</h2>

<p>There are seven valid types:</p>

<blockquote><strong>
<p>VideoPlayer/Single</p>
<p>VideoPlayer/Inline590</p>
<p>VideoPlayer/Inline300</p>
<p>VideoLauncher/Slider300x250</p>
<p>VideoLauncher/Playlist300x250</p>
<p>VideoLauncher/Horizontal</p>
<p>VideoLauncher/Vertical</p>
</strong></blockquote>

<h2>Tracking Group</h2>

<p>Your tracking group value is partner specific and is required in order to ensure correct revenue distribution. You can find your tracking group number in the NDN Control Room.</p>

<h2>Widget ID</h2>

<p>When you build a product in the NDN Control Room it is assigned a Widget ID. This ID is used to store and retrieve settings for your product.</p>

<p>There are two global Widget IDs available as well.</p>

<p>To set your player to begin playing on page-load, set the Widget ID to 1.</p>

<p>To set your player to require user interaction to begin playing, set the Widget ID to 2.</p>

<h2>Optional settings</h2>

<blockquote>
<p><strong>id</strong> - A div id to be used for custom CSS styling</p>
<p><strong>class</strong> - Class(es) to be used for custom CSS styling</p>
<p><strong>style</strong> - Used to set width and height in this format: width:###px;height:###px;</p>
<p><strong>videoid</strong> - In a player product this will designate what video to play. This can be found in the NDN Control Room</p> 
<p><strong>playlistid</strong> - To set a playlist from the NDN Control Room in any of the products</p>
<p><strong>sitesection</strong> - A unique value for the player to track views of a specific implementation</p>
</blockquote>

<h2>Set Defaults</h2>

<p>If you want to set default values for your NDN plug-in, you may use the form below.</p>

<p>An example use would be if you know you will mostly be using Single players that will all be 600px wide, 340px high and autoplay and you only want to pass in the Video ID. You would set the default Type to "VideoPlayer/Single", Widget ID to "1", Width to "600", Height to "300", and Tracking Group to your unique value from the NDN Control Room.</p>

<h2>Shortcode Format</h2>
<p>Using the defaults your shortcode to put a video on the page would look like this (with the "########" replaced with the Video ID from the NDN Control Room):</p> <blockquote><strong>[NDN videoid=########]</strong> .</blockquote>

<p>If you didn't set the defaults your embed code would look like this:</p><blockquote><strong>[NDN type=VideoPlayer/Single widgetid=1 trackinggroup=##### videoid=######## style=width:600px;height:300px;]</strong></blockquote>

<form id="ndn_defaults" name="ndn_defaults" method="post" action="<?php __FILE__ ?>">

<label><h3>Default Type:</h3><select name="default_type"><option value="">Choose one</option><option value="VideoPlayer/Single" <?php echo get_option('ndn_default_type')=='VideoPlayer/Single'?'selected="selected"':'' ?>>VideoPlayer/Single</option><option value="VideoPlayer/Inline590" <?php echo get_option('ndn_default_type')=='VideoPlayer/Inline590'?'selected="selected"':'' ?>>VideoPlayer/Inline590</option><option value="VideoPlayer/Inline300" <?php echo get_option('ndn_default_type')=='VideoPlayer/Inline300'?'selected="selected"':'' ?>>VideoPlayer/Inline300</option><option value="VideoLauncher/Slider300x250" <?php echo get_option('ndn_default_type')=='VideoLauncher/Slider300x250'?'selected="selected"':'' ?>>VideoLauncher/Slider300x250</option><option value="VideoLauncher/Playlist300x250" <?php echo get_option('ndn_default_type')=='VideoLauncher/Playlist300x250'?'selected="selected"':'' ?>>VideoLauncher/Playlist300x250</option><option value="VideoLauncher/Horizontal" <?php echo get_option('ndn_default_type')=='VideoLauncher/Horizontal'?'selected="selected"':'' ?>>VideoLauncher/Horizontal</option><option value="VideoLauncher/Vertical" <?php echo get_option('ndn_default_type')=='VideoLauncher/Vertical'?'selected="selected"':'' ?>>VideoLauncher/Vertical</option></select></label>
<label><h3>Default Tracking Group:</h3><input type="text" form="ndn_defaults" name="default_trackinggroup" value="<?php echo get_option('ndn_default_trackinggroup'); ?>"></input></label>
<label><h3>Default Widget ID:</h3><input type="text"  form="ndn_defaults" name="default_widgetid" value="<?php echo get_option('ndn_default_widgetid'); ?>"></input></label>
<label><h3>Default DIV ID (for CSS):</h3><input type="text"  form="ndn_defaults" name="default_id" value="<?php echo get_option('ndn_default_id'); ?>"></input></label>
<label><h3>Default DIV Class (for CSS):</h3><input type="text"  form="ndn_defaults" name="default_class" value="<?php echo get_option('ndn_default_class'); ?>"></input></label>
<label><h3>Default SiteSection:</h3><input type="text"  form="ndn_defaults" name="default_sitesection" value="<?php echo get_option('ndn_default_sitesection'); ?>"></input></label>
<p><strong>The following is for Single Embed (non-responsive) only. If you want a responsive player please add styles with your template CSS.</strong></p>
<label><h3>Default Width:</h3><input type="text"  form="ndn_defaults" name="default_width" value="<?php echo get_option('ndn_default_width'); ?>"></input></label>
<label><h3>Default Height:</h3><input type="text"  form="ndn_defaults" name="default_height" value="<?php echo get_option('ndn_default_height'); ?>"></input></label>

<?php submit_button(); ?>

</form>
</div>
<?php } ?>