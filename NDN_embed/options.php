<div class="wrap">
<?php screen_icon(); ?>
<h2>NDN embed code conversion</h2>

<?php
	if($_POST['submit'] && $_POST['embedcode']){
		
		$error = "<br /><br />This does not appear to be valid embed code, please check your code and try again.<br /><br />";	
		$str = $_POST['embedcode'];
		$class = $_POST['classname'];
		$class = preg_replace('/[\s]/',',',$class);
		if ($class == "ndn_embed"){unset($class);}
		$id = $_POST['idname'];
		// determine if it is iframe
		if (strpos($str, '<div') === false){
			$str = substr($str, ($pos = strpos($str, '<iframe')) !== false ? $pos + 1 : 0);
			$str = substr($str,0,strpos($str,">"));
			$str = preg_replace('/[^\w-:;=?&\/ ]/', '', $str);
			$str = preg_replace('/[&?]/',' ',$str);
			preg_match_all("/([^,= ]+)=([^,= ]+)/", $str, $r); 
			if ($r[1] && $r[2]){
				$result = array_combine($r[1], $r[2]);
				
				if (!$result["widgetId"] && !$result["type"] && !$result["trackingGroup"]){
					print $error;	
				}else{
					$ndn = '[NDN ';
					if ($class){$ndn = $ndn.'class='.$class.' ';}
					if ($id){$ndn = $ndn.'id='.$id.' ';}
					if ($result["width"] && $result["height"]){
						$ndn = $ndn.'style="width:'.$result["width"].'px;height:'.$result["height"].'px;" ';
					}else if ($result["width"]){
						$ndn = $ndn.'style="width:'.$result["width"].';" ';
					}else if ($result["height"]){
						$ndn = $ndn.'style="height:'.$result["height"].';" ';
					}
					if ($result["widgetId"]){$ndn = $ndn.'widgetid='.$result["widgetId"].' ';}
					if ($result["type"]){$ndn = $ndn.'type='.$result["type"].' ';}
					if ($result["videoId"]){$ndn = $ndn.'videoid='.$result["videoId"].' ';}
					if ($result["playlistId"]){$ndn = $ndn.'playlistid='.$result["playlistId"].' ';}
					if ($result["siteSection"]){$ndn = $ndn.'sitesection='.$result["siteSection"].' ';}
					if ($result["trackingGroup"]){$ndn = $ndn.'trackinggroup='.$result["trackingGroup"].' ';}
					$ndn = $ndn.']';
					
					print "<br /><br />Copy the following short code and paste it into your page or post:<br /><br />".$ndn."<br /><br />";
		
				}
			}else{
				print $error;
			}
			
		}else{
			$str = substr($str, ($pos = strpos($str, '<div')) !== false ? $pos + 1 : 0);
			$str = substr($str,0,strpos($str,">"));
			$str = preg_replace('/[^\w-:;=\/ ]/', '', $str);
			preg_match_all("/([^,= ]+)=([^,= ]+)/", $str, $r); 
			if ($r[1] && $r[2]){
				$result = array_combine($r[1], $r[2]);
				
				if (!$result["data-config-widget-id"] && !$result["data-config-type"] && !$result["data-config-tracking-group"]){
					print $error;	
				}else{
					$ndn = '[NDN ';
					if ($result["class"] && $result["class"] != "ndn_embed"){$ndn = $ndn.'class='.$class.' ';}
					if ($id){$ndn = $ndn.'id='.$id.' ';}
					if ($result["style"]){$ndn = $ndn.'style='.$result["style"].' ';}
					if ($result["data-config-widget-id"]){$ndn = $ndn.'widgetid='.$result["data-config-widget-id"].' ';}
					if ($result["data-config-type"]){$ndn = $ndn.'type='.$result["data-config-type"].' ';}
					if ($result["data-config-video-id"]){$ndn = $ndn.'videoid='.$result["data-config-video-id"].' ';}
					if ($result["data-config-playlist-id"]){$ndn = $ndn.'playlistid='.$result["data-config-playlist-id"].' ';}
					if ($result["data-config-site-section"]){$ndn = $ndn.'sitesection='.$result["data-config-site-section"].' ';}
					if ($result["data-config-tracking-group"]){$ndn = $ndn.'trackinggroup='.$result["data-config-tracking-group"].' ';}
					if ($result["data-config-rmm-sound-on"]){$ndn = $ndn.'rmmsoundon='.$result["data-config-rmm-sound-on"].' ';}
					if ($result["data-config-auto-play"]){$ndn = $ndn.'autoplay='.$result["data-config-auto-play"].' ';}
					if ($result["data-config-continuous-play"]){$ndn = $ndn.'continuousplay='.$result["data-config-continuous-play"].' ';}
					$ndn = $ndn.']';
					
					print "<br /><br />Copy the following short code and paste it into your page or post:<br /><br />".$ndn."<br /><br />";
		
				}
			}else{
				print $error;
			}
		}
	}
?>

<form id="ndn_convert" action="" method="POST">
<label><h3>Copy your Embed code from the <a href="http://control.newsinc.com/" target="_blank">NDN Control room</a> or Social Share screen on a player here:</h3><textarea form="ndn_convert" cols="40" rows="5" name="embedcode"></textarea></label><br />
<h2>In order to make a single player responsive you need to control it with your own CSS. You can add classes and/or a DIV id below to target in your CSS file.</h2>
<label><h3>Enter your custom CSS Class Name(s) here:<br />(optional - separate by space)</h3><textarea form="ndn_convert" name="classname"></textarea></label><br />
<label><h3>Enter your DIV Id Name here:<br />(optional)</h3><textarea form="ndn_convert" name="idname"></textarea></label><br />
<input name="submit" type="submit" value="Convert" />
</form>

</div>

